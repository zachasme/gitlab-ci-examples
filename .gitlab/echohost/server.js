const http = require("http");
const url = require("url");

const server = http.createServer((request, response) => {
  const { query } = url.parse(request.url, true);

  response.writeHead(200, { "Content-Type": "text/plain" });
  response.write(`Hello, ${query.hostname}!\n`);
  response.end();
});

server.listen(1337, "0.0.0.0", () => {
  console.log("running...");
});
